#base image
FROM openjdk:11-jre-slim-buster
COPY ./target/flocking-1.0-SNAPSHOT.jar  ./
CMD ["java", "-jar", "flocking-1.0-SNAPSHOT.jar"]