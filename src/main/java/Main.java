import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import processing.event.MouseEvent;

//The main function
public class Main extends PApplet {

    private int         previousMillis = 0;
    private PVector     mousePos       = new PVector(0,0);
    public  SpatialGrid grid;

    public void settings() {
        size(1000, 600);
        grid = new SpatialGrid(this,50f);
        PImage image = loadImage("src\\main\\resources\\images\\bird_sprite.png");

        // Creation de quelques oiseaux à des postions aléatoires
        for (int i=0; i<20; ++i)
        {
            PVector randomPosition = new PVector(random(0,width), random(0,height));
            Bird bird = new Bird(this, randomPosition,new Graphic(image,this));
            bird.update(random(0,1));
            grid.birds.add(bird);
        }
    }

    public void draw(){
        // calculer le temps écoulé après le dernier frame (draw s'appelle à chaque frame)
        int millisElapsed = millis() - previousMillis;
        float secondsElapsed = millisElapsed / 1000f;
        previousMillis = millis();

        // style
        background(10);
        fill(89, 169, 249,200);
        rect(-5,-5,width+5,height+5);
        grid.clearGridCells();

        // ajout des agents sur la grille
        for (Bird bird : grid.birds)
            grid.add(bird, bird.position.x, bird.position.y);
        for (Predator predator : grid.predators)
            grid.add(predator, predator.position.x, predator.position.y);

        // Calculer les forces sur les oiseaux
        for (Bird bird : grid.birds)
            bird.calculAcceleration(grid);

        // dessiner les oiseaux
        for (Bird bird : grid.birds)
        {
            bird.update(secondsElapsed);
            bird.draw();
        }

        // Calculer les forces sur les prédateurs
        for (Predator predator : grid.predators)
            predator.calculAcceleration(grid);

        // dessiner les prédateurs
        for (Predator predator : grid.predators)
        {
            predator.update(secondsElapsed);
            predator.draw();
        }

        // Dessiner les obstacles
        for (Obstacle obstacle : grid.obstacles)
            obstacle.draw();
    }

    @Override
    protected void handleMouseEvent(MouseEvent event) {
        super.handleMouseEvent(event);
        final int action = event.getAction();
        if (action == MouseEvent.CLICK)
        {
            pmouseX = emouseX;
            pmouseY = emouseY;
            mouseX = event.getX();
            mouseY = event.getY();
            mousePos = new PVector(mouseX,mouseY);
        }

    }

    public PVector getMousePos() {
        return mousePos;
    }

    public void setMousePos(PVector mousePos) {
        this.mousePos = mousePos;
    }

    public static void main(String... args){
        String[] processingArgs = {"MySketch"};
        Main mySketch = new Main();
        PApplet.runSketch(processingArgs, mySketch);
    }


}
