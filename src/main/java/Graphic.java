import processing.core.PImage;

public class Graphic {

    private PImage  spriteSheet;
    private Main    sketch;
    private float   secondsSinceAnimationStarted    = 0;
    private int     frameCount                      = 3;
    private float   frameDurationSeconds            = 0.1f;
    private int     frameWidth                      = 31;

    public Graphic(PImage spriteSheet, Main sketch) {
        this.spriteSheet = spriteSheet;
        this.sketch = sketch;
    }

    void updateAnim(float secondsElapsed) {
        secondsSinceAnimationStarted += secondsElapsed;
    }

    /**
     * dessine l'element graphic
     */
    void draw() {
        // logique pour determiner quel frame dessiner
        int currentFrameIndex = (int)(secondsSinceAnimationStarted / frameDurationSeconds);
        currentFrameIndex %= frameCount;

        drawAnimationFrame(currentFrameIndex);
    }

    void drawAnimationFrame(int frameIndex) {
        // couper la partie correspondante à dessiner de l'image
        sketch.imageMode(sketch.CENTER);

        int frameStartX = frameWidth*frameIndex;

        sketch.image(
                spriteSheet,
                0, 0,
                frameWidth, frameWidth,
                frameStartX, 0,                        // Top-left
                frameStartX + frameWidth, frameWidth   // Bottom-right
        );
    }
}
