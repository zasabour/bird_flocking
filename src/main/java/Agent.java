import processing.core.PVector;

public class Agent {

    private    Main        sketch;
    private    Graphic     graphic;
    protected  PVector     position;
    protected  PVector     velocity;
    private    PVector     acceleration;
    private    PVector     sepForce;
    private    PVector     alignForce;

    public Agent(Main sketch, PVector position, Graphic graphic){
        this.sketch   = sketch;
        this.graphic  = graphic;
        this.position = position;
        velocity      = new PVector(0,0);
        acceleration  = new PVector(0,0);
        sepForce      = new PVector(0,0);
        alignForce    = new PVector(0,0);
    }

    public void update(float timePassed) {
        // pour l'animation
        graphic.updateAnim(timePassed);

        // Acceleration change la vitesse
        velocity.add(PVector.mult(acceleration, timePassed));

        // Limiter la vitesse
        if (velocity.magSq() > SpatialGrid.AGENT_MAX_SPEED *SpatialGrid.AGENT_MAX_SPEED)
            velocity.setMag(SpatialGrid.AGENT_MAX_SPEED);

        // La vitesse change la position
        position.add(PVector.mult(velocity, timePassed));

        //assurer que les agents ne quittent pas l'écran
        if (position.x >= sketch.width)
            position.x -= sketch.width;
        else if (position.x < 0)
            position.x += sketch.width;

        if (position.y >= sketch.height)
            position.y -= sketch.height;
        else if (position.y < 0)
            position.y += sketch.height;
    }

    /**
     * cette méthode calcule l'acceleration de l'agent mais aussi assure le respect des principes de séparation
     * et alignement aussi que l'évitement des obstacles
     * @param grid la grille
     */
    public void calculAcceleration(SpatialGrid grid){
        acceleration.set(0,0);

        // se diriger vers la position de la souris lors d'un clique
        PVector vectorToTarget = PVector.sub(sketch.getMousePos(), position);
        PVector accelerationTowardsTarget = vectorToTarget.setMag(SpatialGrid.AGENT_MOUSE_FOLLOW_STRENGTH);
        acceleration.add(accelerationTowardsTarget);

        // eviter les obstacles
        for (Obstacle obstacle : grid.obstacles) {
            PVector vectorToObstacle = PVector.sub(obstacle.getPosition(), position);
            float squareDistanceToObstacle = vectorToObstacle.magSq();
            if (squareDistanceToObstacle < SpatialGrid.OBSTACLE_SIZE*SpatialGrid.OBSTACLE_SIZE)
            {
                float distanceToObstacle = (float)Math.sqrt(squareDistanceToObstacle);
                float obstacleAvoidAmount = ((1f-distanceToObstacle/SpatialGrid.OBSTACLE_SIZE) * (1f-distanceToObstacle/SpatialGrid.OBSTACLE_SIZE)) * SpatialGrid.OBSTACLE_AVOID_STRENGTH;
                acceleration.add(vectorToObstacle.setMag(-obstacleAvoidAmount));
            }
        }

        //Separation
        sepForce.set(0,0);
        for (Agent otherBird : grid.queryNeighbors(position.x, position.y, SpatialGrid.AGENT_SEPARATION_RADIUS))
        {
            // ne pas comparer un agent avec lui-meme
            if (otherBird == this)
                continue;

            PVector vectorToOtherBird = PVector.sub(otherBird.position, position);
            float squareDistanceToOtherBird = vectorToOtherBird.magSq();

            // Ignorer si le voisin est loin ( > le carre du separation_radius)
            if (squareDistanceToOtherBird > SpatialGrid.AGENT_SEPARATION_RADIUS *SpatialGrid.AGENT_SEPARATION_RADIUS)
                continue;

            // s'eloigner du voisin
            sepForce.add(vectorToOtherBird.setMag(-SpatialGrid.AGENT_SEPARATION_STRENGTH));
        }
        acceleration.add(sepForce);

        //Alignement
        alignForce.set(0,0);

        PVector averageVelocityOfNeighbours = new PVector(0,0);
        int alignmentNeighbourCount = 0;

        for (Agent otherBird : grid.queryNeighbors(position.x, position.y, SpatialGrid.AGENT_ALIGNMENT_RADIUS))
        {
            if (otherBird == this)
                continue;

            PVector vectorToOtherBird = PVector.sub(otherBird.position, position);
            float squareDistanceToOtherBird = vectorToOtherBird.magSq();

            if (squareDistanceToOtherBird > SpatialGrid.AGENT_ALIGNMENT_RADIUS *SpatialGrid.AGENT_ALIGNMENT_RADIUS)
                continue;

            ++alignmentNeighbourCount;
            averageVelocityOfNeighbours.add(otherBird.velocity);
        }

        if (alignmentNeighbourCount > 0)
        {
            averageVelocityOfNeighbours.mult(1f / alignmentNeighbourCount);
            alignForce.set(averageVelocityOfNeighbours.setMag(SpatialGrid.AGENT_ALIGNMENT_STRENGTH));
            acceleration.add(alignForce);
        }

    }


    /**
     * dessine l'agent
     */
    public void draw() {
        // memoriser l'etat de la grid
        sketch.pushMatrix();

        //rotation
        sketch.translate(position.x, position.y);
        float angle = sketch.atan2(velocity.y, velocity.x);
        sketch.rotate(angle);
        graphic.draw();

        // revenir à l'etat enregistré de la grid
        sketch.popMatrix();
    }



}
